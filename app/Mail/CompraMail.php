<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CompraMail extends Mailable
{
    use Queueable, SerializesModels;

    public $data;

    public function __construct($compra)
    {
        $this->data = $compra;
    }

    public function build()
    {
        $address = 'devfernando95@gmail.com';
        $subject = 'Mensaje de Comfirmacion de su Compra!';
        $name = 'Elaniin';

        return $this->view('emails.mailConfirmCompra')
                    ->from($address, $name)
                    ->cc($address, $name)
                    ->bcc($address, $name)
                    ->replyTo($address, $name)
                    ->subject($subject)
                    ->with([ 'message' => $this->data]);
    }
}

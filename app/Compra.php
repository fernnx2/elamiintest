<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Compra extends Model
{
    protected $table = 'compras';
    protected $primaryKey ='id';

    protected $fillable = ['id','cliente_id','producto_id'];

    public function cliente(){
        return $this->belongsTo('App\Cliente');
    }

    public function producto(){
        return $this->belongsTo('App\Producto');
    }

}

<?php namespace App\Http\Controllers;

use App\Compra;
use Exception;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;
use App\Http\Controllers\ConfirmCompraController;

class CompraController extends Controller
{

    /**
     * Get
     */
    public function index(){
        try{
            $compra = Compra::with('cliente','producto')->get();
            return response()->json($compra,200);

        }catch(Exception $e){
            return response()->json(["Error no found resource"=>$e],400);
        }
    }

    /**
     * Get @param id
     */
    public function find($factura){
        try{
            $compra = Compra::with(['cliente','producto'])->where('factura',$factura)->get();
            return response()->json($compra,200);
        }catch(Exception $e){
            return response()->json(["Error not found Resource"=>$e],400);
        }
    }

    /**
     * Post @param email
     */
    public function create(Request $request){
        try{
            $factura =Uuid::uuid1();
            $data = \json_decode($request->getContent(), true);
            for ($i = 0; $i < sizeof($data); $i++) {
                $compra = new Compra();
                $compra->factura = $factura;
                $compra->cliente_id = $data[$i]['cliente_id'];
                $compra->producto_id = $data[$i]['product_id'];
                $compra->cantidad = $data[$i]['cantidad'];
                $compra->save();

            }
            $send = new ConfirmCompraController();
            $compraTotal = Compra::with(['cliente','producto'])->where('factura',$factura)->get();
            $send->sendEmail($compraTotal[0]['cliente']['id'],$compraTotal);
            return response()->json(["factura"=>$compraTotal],200);
        }catch(Exception $e){
            return response()->json(["Error not created compra"=>$e],400);
        }
    }

    /**
     * PUT @param {id}
     */
    public function update(Request $request,$id){
        try{

        }catch(Exception $e){
            return response()->json(["Error no update compra"=>$e],400);
        }
    }

    /**
     * @param {id}
     */
    public function delete($id){
        try{

        }catch(Exception $e){
            return response()->json(["Error no delete compra"=>$e],400);
        }
    }


}

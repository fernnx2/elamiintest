<?php namespace App\Http\Controllers;

use App\Producto;
use Exception;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    /*
     * Get
     */
    public function index(){
        try{
            $products = Producto::all();
            return response()->json($products,200);

        }catch(Exception $e){
            return response()->json("Error no found resource",400);
        }
    }

    /*
     * Get @Param id
     */
    public function find($id){
        try{
            $product = Producto::find($id);
            return response()->json($product,200);
        }catch(Exception $e){
            return response()->json("Error not found Resource",400);
        }
    }


}

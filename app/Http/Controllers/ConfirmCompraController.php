<?php namespace App\Http\Controllers;

use App\Mail\CompraMail;
use App\Cliente;
use Illuminate\Support\Facades\Mail;
class ConfirmCompraController extends Controller
{

    /**
     * GET @param email
     */
    public function sendEmail($id,$compraTotal){
        try{
            $cliente = Cliente::find($id);
            Mail::to($cliente->email)->send(new CompraMail($compraTotal));
            return response()->json(["Email enviado con exito a:"=>$cliente->email],200);
        }catch(Exception $e){
            return response()->json(["Error"=>$e],400);
        }

}




}



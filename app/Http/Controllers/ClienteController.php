<?php namespace App\Http\Controllers;

use App\Cliente;
use Exception;
use Illuminate\Http\Request;

class ClienteController extends Controller
{


    /**
     * Get @param id
     */
    public function login(Request $request){
        try{
 		$data = json_decode($request->getContent(), true);
            $cliente = Cliente::where('email',$data['email'])->get();
            if($cliente[0]['password']== $data['password']){
 			return response()->json($cliente,200);
            }
            else{
                return response()->json(["Error"=>"Datos invalidos"],401);
            }
        }catch(Exception $e){
            return response()->json(["Error not found Resource"=>$e],400);
        }
    }

    /**
     * Post
     */
    public function create(Request $request){
        try{
            $data = \json_decode($request->getContent(), true);
            $cliente = new Cliente();
            $cliente->nombre = $data['nombre'];
            $cliente->email = $data['email'];
            $cliente->password = $data['password'];
            $cliente->save();
            return response()->json($cliente,200);
        }catch(Exception $e){
            return response()->json(["Error not created client",$e],400);
        }
    }

    /**
     * PUT @param {id}
     */
    public function update(Request $request,$id){
        try{
            $data = \json_decode($request->getContent(), true);
            $cliente = Cliente::find($id);
            $cliente->nombre = $data['nombre'];
            $cliente->email = $data['email'];
            $cliente->password = $data['password'];
            $cliente->save();
            return response()->json($cliente,200);
        }catch(Exception $e){
            return response()->json(["Error no update client"=>$e],400);
        }
    }

    /**
     * @param {id}
     */
    public function delete($id){
        try{
            $cliente = Cliente::find($id);
            $cliente->delete();
            return response()->json("Delete client",200);
        }catch(Exception $e){
            return response()->json(["Error no delete client"=>$e],400);
        }
    }


}

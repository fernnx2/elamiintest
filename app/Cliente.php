<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Cliente extends Model
{
    protected $table = 'clientes';
    protected $primaryKey ='id';

    protected $fillable = ['id','nombre','email','password'];

    public function compras(){
        return $this->hasMany('App\Compra');
    }

}

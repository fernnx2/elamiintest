# Bakcend Elaniin

api minitienda

# Dependencias
    1. php>=7.2
    2. Composer
    3. Mysql >=5.4

# Instalacion

    composer install

    php artisan migrate

    php artisan db:seed

    php -S localhost:8000 -t public


## License

The open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

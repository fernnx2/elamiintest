<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});



//routes for products
$router->get('/producto', 'ProductController@index');
$router->get('/producto/{id}', 'ProductController@find');

//routes for client
$router->post('/cliente/login','ClienteController@login');
$router->post('/cliente','ClienteController@create');
$router->put('/cliente/{id}','ClienteController@update');
$router->delete('/cliente/{id}','ClienteController@delete');

//routes for compras
$router->get('/compra','CompraController@index');
$router->get('/compra/{factura}','CompraController@find');
$router->post('/compra','CompraController@create');




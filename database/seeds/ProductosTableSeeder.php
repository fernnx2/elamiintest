<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ProductosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('productos')->insert([
            'nombre'=> "producto1",
            'precio'=> 10,
             'image'=>"alsace.gif",
             'descripcion'=>Str::random(20)
        ]);
        DB::table('productos')->insert([
            'nombre'=> "producto2",
            'precio'=> 20,
             'image'=>"caribou.gif",
             'descripcion'=>Str::random(20)
        ]);
        DB::table('productos')->insert([
            'nombre'=> "producto3",
            'precio'=> 30,
             'image'=>"colobus.gif",
             'descripcion'=>Str::random(20)
        ]);
    }
}

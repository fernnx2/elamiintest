<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
class ComprasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('compras')->insert([
            'factura' => Str::random(10),
            'cliente_id' => 1,
            'producto_id'=>1,
            'cantidad'=>5
        ]);
        DB::table('compras')->insert([
            'factura' => Str::random(10),
            'cliente_id' => 1,
            'producto_id'=>2,
            'cantidad'=>2
        ]);
        DB::table('compras')->insert([
            'factura' => Str::random(10),
            'cliente_id' => 2,
            'producto_id'=>2,
            'cantidad'=>3
        ]);
        DB::table('compras')->insert([
            'factura' => Str::random(10),
            'cliente_id' => 2,
            'producto_id'=>3,
            'cantidad'=>10
        ]);
    }
}

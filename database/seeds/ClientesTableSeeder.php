<?php

use Illuminate\Database\Seeder;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clientes')->insert([
            'nombre' => "fernando",
            'email' => 'menjivatfernando1995@gmail.com',
            'password' => '12345',
        ]);
        DB::table('clientes')->insert([
            'nombre' => "menjivar",
            'email' => 'devfernando95@gmail.com',
            'password' => '54321',
        ]);
    }
}
